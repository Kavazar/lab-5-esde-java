
public class Main {
    public static void main(String[] args) {
        char[] arrayOfChars = new char[]{'a', 'c', 'b', 'b', '1', '-'};
        CharSorting.showChars(arrayOfChars);
        CharSorting.selectionSorting(arrayOfChars);
        CharSorting.bubbleSorting(arrayOfChars);
        CharSorting.insertionSorting(arrayOfChars);
        System.out.println("---------------------------------------");

        String[] arrayOfStrings = new String[]{"a", "c", "bb", "b", "1", "-"};
        StringSorting.showStrings(arrayOfStrings);
        StringSorting.selectionSorting(arrayOfStrings);
        StringSorting.bubbleSorting(arrayOfStrings);
        StringSorting.insertionSorting(arrayOfStrings);
        System.out.println("---------------------------------------");

           String[] names = new String[]{"Andrey", "Dima", "Bob", "Denis", "Bob", "Anna"};
        String[] surnames = new String[]{"1"     , "6"   , "4"  , "5"    , "3"  , "2"};
        PeopleSorting.showPeople(names, surnames);
        PeopleSorting.sorting(names, surnames);
    }
}