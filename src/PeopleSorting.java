public class PeopleSorting {
    public static void showPeople(String[] names, String[] surnames) {
        System.out.println("Array of names:");
        for (int i = 0; i < names.length; i++) {
            System.out.print(names[i] + " ");
        }
        System.out.println("");
        System.out.println("Array of surnames:");
        for (int i = 0; i < surnames.length; i++) {
            System.out.print(surnames[i] + " ");
        }
        System.out.println("");
    }

    public static void sorting(String[] names, String[] surnames) {
        for (int i = 0; i < names.length; i++) {
            for (int j = 0; j < names.length - 1; j++) {
                int res = names[j].compareTo(names[j + 1]);
                if (res == 0) {
                    res = surnames[j].compareTo(surnames[j + 1]);
                }

                if (res > 0) {
                    String temp = names[j];
                    names[j] = names[j + 1];
                    names[j + 1] = temp;
                    temp = surnames[j];
                    surnames[j] = surnames[j + 1];
                    surnames[j + 1] = temp;
                }
            }
        }
        System.out.println("Sorted array of names:");
        for (int i = 0; i < names.length; i++) {
            System.out.print(names[i] + " ");
        }
        System.out.println("");
        System.out.println("Sorted array of surnames:");
        for (int i = 0; i < surnames.length; i++) {
            System.out.print(surnames[i] + " ");
        }
        System.out.println("");
    }
}
