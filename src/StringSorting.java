public class StringSorting {
    public static void showStrings(String[] unsortedArray) {
        System.out.println("Unsorted array of Strings:");
        for (int i = 0; i < unsortedArray.length; i++) {
            System.out.print(unsortedArray[i] + " ");
        }
        System.out.println("");
    }

    public static void selectionSorting(String[] unsortedArray) {
        String[] array = unsortedArray;
        for (int i = 0; i < array.length - 1; ++i) {
            int minPos = i;
            for (int j = i + 1; j < array.length; ++j) {
                if (array[j].compareTo(array[minPos]) < 0) {
                    minPos = j;
                }
            }
            String temp = array[minPos];
            array[minPos] = array[i];
            array[i] = temp;
        }
        System.out.println("Array sorted by selection sort:");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("");
    }

    public static void bubbleSorting(String[] unsortedArray) {
        String[] array = unsortedArray;
        boolean isSwapped = true;
        while (isSwapped) {

            isSwapped = false;
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j].compareTo(array[j + 1]) > 0) {
                    String temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    isSwapped = true;
                }
            }
        }
        System.out.println("Array sorted by bubble sort:");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("");
    }

    public static void insertionSorting(String[] unsortedArray) {
        String[] array = unsortedArray;
        for (int i = 1; i < array.length; i++) {
            int j = i - 1;
            String temp = array[i];
            while (j >= 0 && array[j].compareTo(temp) > 0) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = temp;
        }
        System.out.println("Array sorted by insertion sort:");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("");
    }
}
